import axios from 'axios'
import qs from 'qs'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

/*global BUILD_ENV */
const baseURL = {
  local: 'http://10.10.80.44:11017', // 禹蕾 接口服务器
  // local: 'http://10.10.80.44:10005', // 新亮 接口服务器
  dev: '', // 自己测试 fex deploy dev
  test: 'https://test-polaris-api.apuscn.com', // qa测试 fex deploy dev
  prod: 'https://polaris-api.apuscn.com'
}

// 创建axios实例
const service = axios.create({
  withCredentials: true,
  // timeout: 30000 // 请求超时时间
  baseURL: baseURL[BUILD_ENV]
})

// request拦截器
service.interceptors.request.use(config => {
  if (config.method === 'post' || config.method === 'put') {
    config.data = qs.stringify(config.data)
  }
  NProgress.start()
  return config
}, error => {
  NProgress.done()
  Promise.reject(error)
})

// respone拦截器
service.interceptors.response.use(res => {
  if (res.data && res.data.code * 1 === 302) {
    const url = res.data.data
    window.location.href = url
  }
  NProgress.done()
  return res
}, error => {
  NProgress.done()
  return Promise.reject(error)
})

export default service
