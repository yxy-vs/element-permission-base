import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import user from './modules/user'
import permission from './modules/permission'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    user,
    permission
  },
  getters: {
    sidebar: state => state.app.sidebar,
    device: state => state.app.device,
    permission_routers: state => state.permission.routers
  }
})

export default store
