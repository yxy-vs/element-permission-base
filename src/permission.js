import router, {
  // constantRouterMap,
  asyncRouterMap,
  matchAll
} from '@/router'
import store from '@/store'

router.addRoutes(asyncRouterMap, matchAll)
// router.addRoutes(asyncRouterMap)

store.commit('SET_ROUTERS', asyncRouterMap)
